import Box from "./Box";
import { useState, useContext } from "react";


function App() {

  let gameSizeHorizontal = 10,
    gameSizeVertical = 10;

  let playingField = [];

  for (let i = 0; i < gameSizeHorizontal; i++) {
    playingField[i] = [];
    for (let j = 0; j < gameSizeVertical; j++) {
      playingField[i][j] = {};
    }
  }

  /*
  const PLAYER = {
    PLAYER1,
    PLAYER2,
  }

  function reducer(state, action) {
    switch (action.type) {
      case PLAYER.PLAYER1:
        return {state: "red"}
      case PLAYER.PLAYER2:
        return {state: "blue"}
      default:
        return state
    }
  }
  */

  console.log(playingField);

  return (
    <header>
      <div className="container">
        <div className="column">
          {playingField.map((x, index) => (
            <div key={index + 1}>
              {x.map((y) => (
                <div>
                  <Box key={y.id} />
                  {console.log(y.id)}
                </div>
              ))}
            </div>
          ))}
        </div>
      </div>
    </header>
  );
}

export default App;
