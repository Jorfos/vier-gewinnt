import { useState, useReducer } from "react";

const Box = ({ boxID }) => {

  const [player, changePlayer] = useReducer()

  const [box, setBox] = useState({
    colour: "white",
    height: 5,
    width: 5,
  });

  // const [player, setPlayer] = useState({
  //   player : "red",
  // });

  const { colour, height, width } = box;

  // const { player } = player;

  const onClick = (e) => {
    const { backgroundColor } = e.target.style;

    if (backgroundColor === "white") {
      setBox({ ...box, colour: e.target.player });
      //setplayer({player : "blue"})
    }

    console.log(e.target.style);
  };

  return (
    <div
      className="box"
      style={{ backgroundColor: colour, height, width }}
      //player={{ player: "white" }}
      onClick={onClick}
    ></div>
  );
};

export default Box;
